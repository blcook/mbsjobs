'use strict';

var INDEED_BASE_URL = 'http://api.indeed.com/ads/apisearch?';

import http from 'http';
import querystring from 'querystring';

import Search from './search.model';

/**
 * Get job data from indeed
 */
export function index(req, res) {

  var newSearch = new Search();
  var ipAddress = req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;

  if (ipAddress.indexOf(',') > -1) {
    ipAddress = ipAddress.split(',')[0];
  }

  newSearch.ipAddress = ipAddress;
  newSearch.keyword = req.query.keyword;
  newSearch.zipCode = req.query.zipCode;
  newSearch.saveAsync();

  return http.get(INDEED_BASE_URL + querystring.stringify({
      publisher: process.env.INDEED_PUBLISHER,
      chnl: process.env.INDEED_CHNL,
      q: req.query.keyword,
      l: req.query.zipCode,
      start: req.query.start,
      limit: req.query.limit,
      userip: ipAddress,
      useragent: req.headers['user-agent'],
      format: 'json',
      v: '2'
    }), function(response) {
      var data = '';
      response.on('data', function(chunk) {
        data += chunk;
      });
      response.on('end', function() {
        res.status(200).send(data);
      });
    })
    .on('error', function(err) {
      res.status(400).send({ error: err });
    });
}

/**
 * Get all recorded searches
 */
export function all(req, res) {
  Search.findAsync({})
    .then(searches => {
      res.status(200).json(searches);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
}
