'use strict';

angular.module('mbsjobsApp.auth', [
  'mbsjobsApp.constants',
  'mbsjobsApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
