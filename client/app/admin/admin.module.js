'use strict';

angular.module('mbsjobsApp.admin', [
  'mbsjobsApp.auth',
  'ui.router'
]);
