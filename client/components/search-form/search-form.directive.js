(function(angular) {
  'use strict';

  angular
    .module('mbsjobsApp')
    .directive('mbsSearchForm', searchForm);

  /* @ngInject */
  function searchForm() {
    let directive = {
      restrict: 'AE',
      templateUrl: 'components/search-form/search-form.html',
      controller: 'SearchFormController',
      controllerAs: 'vm',
      bindToController: true,
      scope: {
        keyword: '=?',
        zipCode: '=?'
      }
    };

    return directive;
  }

})(window.angular);
