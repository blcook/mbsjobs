(function(angular) {
  'use strict';

  angular
    .module('mbsjobsApp')
    .factory('Code', Code);

  /* @ngInject */
  function Code($http, $log) {
    let service = {
      get,
      put
    };

    return service;

    function get() {
      return $http.get('/api/code')
        .then((res) => {
          return res.data;
        })
        .catch((err) => {
          return $log.error(err);
        });
    }

    function put(blob) {
      return $http({
        url: '/api/code',
        method: 'PUT',
        data: {
          blob
        }
      })
      .catch((err) => {
        return $log.error(err);
      });
    }
  }

})(window.angular);
