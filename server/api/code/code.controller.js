'use strict';

import Code from './code.model';

/**
 * Get tracking code
 */
export function index(req, res) {
  Code.findOneAsync({ id: 'analytics' })
    .then(code => {
      res.status(200).json(code);
    })
    .catch(err => {
      res.status(400).json({ error: err });
    });
}

/**
 * Updates analytics tracking code
 */
export function put(req, res) {
  Code.findOneAsync({ id: 'analytics' })
    .then(code => {
      code.blob = req.body.blob;
      return code.saveAsync()
        .then(() => {
          res.status(204).end();
        })
        .catch(err => {
          res.status(400).send({ error: err });
        });
    })
}
