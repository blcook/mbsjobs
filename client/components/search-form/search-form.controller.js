(function(angular) {
  'use strict';

  angular
    .module('mbsjobsApp')
    .controller('SearchFormController', SearchFormController);

  /* @ngInject */
  function SearchFormController($state) {
    let vm = this;

    vm.submit = submit;

    function submit() {
      $state.go('search', {
        keyword: vm.keyword,
        zipCode: vm.zipCode
      });
    }
  }

})(window.angular);
