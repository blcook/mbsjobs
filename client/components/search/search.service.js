(function(angular) {
  'use strict';

  angular
    .module('mbsjobsApp')
    .factory('Search', Search);

  /* @ngInject */
  function Search($http, $log) {
    let service = {
      all
    };

    return service;

    function all() {
      return $http.get('/api/search/all')
        .then((res) => {
          return res.data;
        })
        .catch((err) => {
          return $log.error(err);
        });
    }
  }

})(window.angular);
