'use strict';

import {Router} from 'express';
import * as controller from './code.controller';
import * as auth from '../../auth/auth.service';

var router = new Router();

router.get('/', controller.index);
router.put('/', auth.hasRole('admin'), controller.put);

export default router;
