'use strict';

angular.module('mbsjobsApp', [
  'mbsjobsApp.auth',
  'mbsjobsApp.admin',
  'mbsjobsApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'validation.match'
])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  })
  .run(run);

/* @ngInject */
function run($rootScope, Code, $document) {
  Code.get()
    .then(code => {
      let documentNative = $document.get(0);
      let div = documentNative.createElement('div');
      div.innerHTML = code.blob;
      $document.find('body').append(div);
    });
}
