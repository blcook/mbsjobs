'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
import {Schema} from 'mongoose';

var SearchSchema = new Schema({
  date: Date,
  ipAddress: String,
  keyword: String,
  zipCode: String
});

/**
 * Pre-save hook
 */
SearchSchema
  .pre('save', function(next) {
    this.date = Date.now();
    next();
  });

export default mongoose.model('Search', SearchSchema);
