'use strict';

angular.module('mbsjobsApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('search', {
        url: '/search?zipCode&keyword&start&limit',
        templateUrl: 'app/search/search.html',
        controller: 'SearchController',
        controllerAs: 'vm',
        params: {
          start: '0',
          limit: '10'
        }
      });
  });
