# Design Notes for MBS Jobs

## Search Analytics API

The system should implement the following routes/HTTP methods:

### `POST /api/searches`

This is the main route used by non-administrative visitors to the web app. When a user searches for jobs, a request is sent to this route. Our server then proxies to the Indeed API to get results, measuring the response time of the request to Indeed.

As soon as a response is received, the job data from Indeed is sent to the user. Then, our server asyncronously creates a new search object and stores it in a MongoDB database.

In the request body, the route expects the following parameters:

+ `search_terms`: comma-separated list of search terms
+ `location_term`: location entered by user with search

Both parameters allow empty string values. On the server, the requester's IP address is recorded. The search object is assigned an ID on the server.

### `GET /api/searches`

Returns all search objects filtered by the following query parameters:

+ `search_terms`: array of search terms (union of results)
+ `location_terms`: array of location terms (union of results)
+ `min_response_time`: minimum Indeed API response time
+ `max_response_time`: maximum Indeed API response time
+ `search_time_start`: start of range by which to filter searches (if null, return most recent records)
+ `search_time_end`: end of range by which to filter searches (default: now)
+ `ip_address`: IP address (if requested by users, could allow IP addresses with wildcards or ranges (e.g., '123.45.6.*' or '123.45.6.77-88'))
+ `start`: paging field (defaults to 0)
+ `limit`: max number of results to return (default: 1000, max: 100000) -- default and max are illustrative; they could be different, but some max and default are necessary to prevent excessively expensive queries

### `GET /api/searches/count`

Same as the `GET /api/searches` route, but returns only counts of results. `start` and `limit` query parameters are ignored.

### `GET /api/searches/max`
### `GET /api/searches/min`

These routes use the metadata collections in the database to return metadata. The following query parameters are expected:

+ `key`: required, can be `search`, `location`, or `response_time`, indicating the field to get metadata on
+ `start`: paging field (defaults to 0)
+ `limit`: max number of results to return -- a default and max value must be defined

__NOTE: For old data, there may be some limits on the granularity of data available given the data structure described below.__

Other query params (`search_time_start`, `search_time_end`, `min_response_time`, `max_response_time`, `location_terms`, `search_terms`) can be used to filter results. For example, assume this request is made:

`GET /api/searches/max?key=location&search_time_start=2016-01-01T00:00:00.000Z&search_end_time=2016-02-01T00:00:00.000Z&limit=10&search_terms=JavaScript`

The route would return the 10 location terms used the most often in combination with the 'JavaScript' search term during January 2016.

### `GET /api/searches/avg`

This route is very similar to the `max` and `min` routes except that this query can only be run with `key=reponse_time`, since this is the only numeric field that could be averaged.

### `GET /api/searches/{id}`

Returns a search object matching the ID.


## Search Object Schema

The following pseudocode represents a search object stored in the database:

```
{
  _id: hashed combination of IP address and search timestamp,
  search_terms: [ array of trimmed strings ],
  location_term: string,
  search_time: UTC datetime,
  response_time: Indeed API reponse time in ms,
  ip_address: searcher's IP address
}
```

To support faster searchs on the fields most likely to be queried, all fields (except the unique id) should be defined as indexes. 

The increased processing needed to index fields will not affect perceived performance for non-administrative users, since creating and storing a new search object to the database can be resolved asyncronously, even as a user receives results for their search. However, it will yield performance benefits for administrative users querying the database.

The relatively small size of a search object document will maximize storage space.

To support more efficient processing of the most commonly run queries, automated jobs can be run on an hourly and daily bases. These jobs can populate other collections for various historical timeframes. 

For example, a nightly job can populate a collection in which each document represents metadata for that day going back 1 year.

Here is an example of a document from one of these collections:

```
{
  _id: UTC timestamp representing a single hour or day,
  searchs: {
    search_term: {
      _id: search term concatenated with location term,
      location_term: a string location term used with the search_term,
      average_response_time: Indeed server response time,
      count: a number indicating the number of times the search term was entered with the location term
    },
    ...
  },
  location_terms: {
    location_term: {
      _id: location term concatenated with search term,
      search_term: a string single search term used with the location_term,
      average_reponse_time: Indeed server response time,
      count: count of number of times location term used with the search term
    },
    ...
  },
  totals: {
    average_reponse_time: Indeed server response time,
    count: total count of searches in timeframe of document
  }
}
```

There also need to be periodic, automated jobs that delete old records, both from the collections of metadata documents and from the base collection of searches.
