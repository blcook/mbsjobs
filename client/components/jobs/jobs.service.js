(function(angular) {
  'use strict';

  angular
    .module('mbsjobsApp')
    .factory('Jobs', Jobs);

  /* @ngInject */
  function Jobs($http, $log) {
    let service = {
      get
    };

    return service;

    function get(keyword, zipCode, start, limit) {
      return $http({
          url: '/api/search',
          method: 'GET',
          params: {
            keyword,
            zipCode,
            start,
            limit
          }
        })
        .then((res) => {
          return res.data;
        })
        .catch((err) => {
          return $log.error(err);
        });
    }
  }

})(window.angular);
