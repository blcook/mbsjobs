(function(angular, _) {
  'use strict';

  angular
    .module('mbsjobsApp')
    .controller('SearchController', SearchController);

  /* @ngInject */
  function SearchController($stateParams, Jobs) {
    let vm = this;

    vm.searchedKeyword = vm.keyword = $stateParams.keyword;
    vm.searchedZipCode = vm.zipCode = $stateParams.zipCode;
    vm.start = parseInt($stateParams.start);
    vm.limit = parseInt($stateParams.limit);

    vm.page = 1 + Math.floor(vm.start / vm.limit);

    Jobs.get(vm.keyword, vm.zipCode, vm.start, vm.limit)
      .then((data) => {
        vm.pageRange = _.range(
          Math.max(1, vm.page - 2),
          Math.min(vm.page + 3, Math.ceil(data.totalResults / vm.limit))
        );
        vm.jobs = data;
      });
  }

})(window.angular, window._);
