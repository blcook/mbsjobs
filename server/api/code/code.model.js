'use strict';

var mongoose = require('bluebird').promisifyAll(require('mongoose'));
import {Schema} from 'mongoose';

var CodeSchema = new Schema({
  id: {
    type: String,
    default: 'analytics',
    unique: true
  },
  blob: String
});

export default mongoose.model('Code', CodeSchema);
