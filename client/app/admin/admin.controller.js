'use strict';

(function() {

class AdminController {
  constructor(Search, Code) {
    let vm = this;
    vm.Code = Code;
    Search.all()
      .then((data) => {
        vm.searches = data;
      });
    Code.get()
      .then((data) => {
        vm.code = data.blob;
      })
      .catch(() => {
        vm.code = '';
      });
  }

  submitCode() {
    let vm = this;
    vm.Code.put(vm.code);
  }
}

angular.module('mbsjobsApp.admin')
  .controller('AdminController', AdminController);

})();
